# OpenML dataset: Disney-Plus-Movies-and-TV-Shows

https://www.openml.org/d/43392

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Please, If you enjoyed this dataset, don't forget to upvote it.

Content
This dataset contains a couple of shows and series are available on Disney+ stream service. Also, this dataset contains Internet Mobie Database (IMDb) ratings that can provide many interesting insights.

Acknowledgements

The dataset is collected from Flixable.
The dataset is updated monthly, every 1st day.


Inspiration

What is the content available in different countries?
Are there similar content by genre, writer or director?
Are there similar plot by genre?
Which was the biggest title that won most awards?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43392) of an [OpenML dataset](https://www.openml.org/d/43392). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43392/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43392/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43392/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

